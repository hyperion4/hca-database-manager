import logging
import pandas as pd
pd.options.display.width = 200

TABLE_SEARCH_MAPPING = {
    'game_info': 'game_id',
    'player_stats': 'player_id',
    'team_stats': 'team_id',
    'player_data': 'player_id',
    'team_data': 'team_id',
    'officials': 'game_id',
    'goalie_stats': 'player_id',
    'three_stars': 'game_id',
    'scratches': 'player_id',
    'game_events': 'game_id',
}


class BaseContext:
    data = None
    table_name = None

    def __init__(self, data, table_name, logger=None):
        self.logger = logger or logging.getLogger(__name__)
        self.data=data
        self.table_name = table_name

    def __repr__(self):
        return str(self.data.head())

    def __getitem__(self, item):
        return self.data[self.data[TABLE_SEARCH_MAPPING[self.table_name]] == item]
