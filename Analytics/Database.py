from Analytics import DataContexts
from Hipcheck.Datastore import HipcheckDatastore
import logging


class Analytics(HipcheckDatastore):

    def __init__(self, filename=None, logger=None):
        self.contexts = {
            'game_info': DataContexts.GameInfoContext,
            'player_stats': DataContexts.PlayerStatsContext,
            'team_stats': DataContexts.TeamStatsContext,
            'player_data': DataContexts.PlayerDataContext,
            'team_data': DataContexts.TeamDataContext,
            'officials': DataContexts.OfficiatedContext,
            'goalie_stats': DataContexts.GoalieStatsContext,
            'three_stars': DataContexts.ThreeStarsContext,
            'scratches': DataContexts.ScratchesContext,
            'game_events': DataContexts.GameEventsContext,
        }
        self.logger = logger or logging.getLogger(__name__)
        super().__init__(filename, self.logger)
        self.create_contexts()

    def __repr__(self):
        return 'Analytics({})'.format(self.database_path)

    def create_contexts(self):
        for table_name in self.contexts:
            data = super().__getitem__(table_name)
            context = self.contexts[table_name]
            self.contexts[table_name] = context(data, table_name)
