from Analytics.BaseContext import BaseContext
import logging


class GameInfoContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'game_info')


class PlayerStatsContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'player_stats')


class TeamStatsContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'team_stats')


class PlayerDataContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'player_data')


class TeamDataContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'team_data')


class OfficiatedContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'officials')


class GoalieStatsContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'goalie_stats')


class ThreeStarsContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'three_stars')


class ScratchesContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'scratches')


class GameEventsContext(BaseContext):

    def __init(self, data):
        super.__init__(data, 'game_events')
