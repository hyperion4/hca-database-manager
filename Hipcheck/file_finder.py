""" Utility class for finding files within a directory, and filtering by an
extension.

Examples:
    # Returns all files in the current directory by defualt
    FileFinder.list_files()
    # Returns all .db files in \\Databases
    FileFinder.list_files(path='.\\Databases', extension='.db')
"""
import os
import logging

def list_files(path='.', extension='', loger=None):
    """ Return a list of files within path with extension. """
    logger = loger or logging.getLogger(__name__)
    logger.info('Checking %s for %s files.', path, extension)
    try:
        files = os.listdir(path)
        files = [file for file in files if file.endswith(extension)]
        files = list(map(lambda x: path + '\\' + x, files))
        logger.info('Found files: %s', files)
        return files
    except WindowsError:
        logger.info('Path error: %s', path)
        return []
