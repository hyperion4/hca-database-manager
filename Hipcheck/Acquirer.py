""" Module for acquiring NHL data

Handles gathering statistics from NHLs public game api and stores them in their
raw JSON form. Provides an api builder object to simplify creating the
specificity of the data download.

Examples:
    # Download the first 10 games of the 2016-1017 season
    start = GameApiBuilder().set_season(2016).set_matchtype('season')\
        .set_game_id(1)
    end = GameApiBuilder().set_season(2013).set_matchtype('season')\
        .set_game_id(11)
    game_data = Acquirer(start).download_range(end)
    print(game_data.data[0])

    # Download the Stanely cup championships from 2016-1017
    # If no end is specified, end of season or series is used
    start = GameApiBuilder().set_season(2016).set_matchtype('playoffs')\
        .set_round(4).set_series(1).set_game(4)
    game_data = Acquirer(start).download_range()
    print(game_data.data[0])
"""
import logging
import requests
from Hipcheck import Parser
from Hipcheck import TreadPool

class GameApiBuilder:
    """ Create API calls using the builder pattern. """
    _matchtype_codes = {'preseason': '01',
                        'season': '02',
                        'playoffs': '03'}
    _gamecode = 0
    _season = '0000'
    _matchtype = '00'
    _game_id = '0000'
    _round = '00'
    _series = '0'
    _gamenumber = '0'
    _api_left = 'http://statsapi.web.nhl.com/api/v1/game/'
    _api_right = '/feed/live'


    def __init__(self, gamecode=None, logger=None):
        if gamecode:
            self._gamecode = gamecode
        self.logger = logger or logging.getLogger(__name__)


    def __str__(self):
        return 'gamecode: ' + self.get_gamecode()


    def __repr__(self):
        return 'Game_API_Builder(' + self.get_gamecode() + ')'


    def __eq__(self, other):
        return self.get_gamecode() == other.get_gamecode()


    def __ge__(self, other):
        return self.get_gamecode() >= other.get_gamecode()


    def __le__(self, other):
        return self.get_gamecode() <= other.get_gamecode()


    def __gt__(self, other):
        return not self <= other


    def __lt__(self, other):
        return not self >= other


    def __add__(self, other):
        return int(self.get_gamecode()) + int(other.get_gamecode())


    def __sub__(self, other):
        return int(self.get_gamecode()) - int(other.get_gamecode())


    def copy_other(self, other):
        """
        Set builder state to equal that of the other builder ignoring game code.
        """
        self._season = other._season
        self._matchtype = other._matchtype
        self._game_id = other._game_id
        self._round = other._round
        self._series = other._series
        self._gamenumber = other._gamenumber
        return self


    def get_gamecode(self):
        """ Return the gamecode for the current builder state. """
        self.logger.debug('Getting game code.')
        if self._gamecode != 0:
            return str(self._gamecode)
        self.logger.info('Constructing game code.')
        string = self._season + self._matchtype
        if self._matchtype == '03':
            string += self._round + self._series + self._gamenumber
        else:
            string += self._game_id
        self._gamecode = int(string)
        return string


    def set_gamecode(self, gamecode):
        """ Set gamecode using a number (100000000, 9999999999). """
        if (gamecode >= 1000000000 and gamecode < 10000000000):
            self.logger.info('Game code set to %s', gamecode)
            self._gamecode = gamecode
        else:
            self.logger.info('Cannot set game code to %s', gamecode)
        return self


    def set_season(self, season):
        """ Set season using a number (1000, 9999). """
        if (season >= 1000 and season < 10000):
            self.logger.info('Season set to %s', season)
            self._season = str(season)
        else:
            self.logger.info('Cannot set season to %s', season)
        return self


    def set_matchtype(self, matchtype):
        """ Set matchtype using ['preseason', 'season', 'playoffs']. """
        if matchtype in GameApiBuilder._matchtype_codes:
            self._matchtype = GameApiBuilder._matchtype_codes[matchtype]
            self.logger.info('Match type set to %s', self._matchtype)
        else:
            self.logger.info('Cannot set match type to %s', matchtype)
        return self


    def set_game_id(self, game_id):
        """ Set game ID using a number (1 and 9999). """
        if (game_id > 0 and game_id < 10000):
            self.logger.info('Game ID set to %s', game_id)
            temp = str(game_id)
            pad = 4 - len(temp)
            for _ in range(pad):
                temp = '0' + temp
            self._game_id = temp
        else:
            self.logger.info('Cannot set game ID to %s', game_id)
        return self


    def set_round(self, _round):
        """ Set round using a number (1 and 4). """
        if (_round > 0 and _round < 5):
            self.logger.info('Round set to %s', _round)
            self._round = '0' + str(_round)
        else:
            self.logger.info('Cannot set round to %s', _round)
        return self


    def set_series(self, series):
        """ Set series using a number (1 and 8). """
        if (series > 0 and series < 9):
            self.logger.info('Series set to %s', series)
            self._series = str(series)
        else:
            self.logger.info('Cannot set series to %s', series)
        return self


    def set_game(self, game):
        """ Set game using a number (1 and 7). """
        if (game > 0 and game < 8):
            self.logger.info('Game set to %s', game)
            self._gamenumber = str(game)
        else:
            self.logger.info('Cannot set game to %s', game)
        return self


    def increment_game(self):
        """ Increment the current game ID. """
        self.logger.debug('Incrementing gamecode')
        self._gamecode += 1


    def build_api_call(self):
        """ Build api call based on the current builder state. """
        call = self._api_left + self.get_gamecode() + self._api_right
        self.logger.debug('Built API address %s', call)
        return call


class Acquirer():
    """ Acquire NHL data using their public API. """
    builder = GameApiBuilder()
    data = {}
    status = 0
    ready_data = None


    def __init__(self, builder, logger=None):
        self.builder = builder
        self.logger = logger or logging.getLogger(__name__)


    def get_api(self):
        """ Builds an api call using the current builder state. """
        self.logger.debug('Getting API address')
        return self.builder.build_api_call()


    def download_data(self, url=None, count=0):
        """ Download NHL data using current builder. """
        self.logger.debug('Downloading data.')
        if url is None:
            url = self.builder.build_api_call()
        response = requests.get(url).json()
        if 'message' in response: # api call was bad
            self.logger.debug('Got a bad api call.')
            self.status = 404
        else:
            if response['gameData']['status']['detailedState'] == 'Final':
                self.logger.debug('Recieved data.')
                self.data[count] = response
                self.status = 202
        return self


    def download_range(self, end=None):
        """ Download a range of NHL data. """
        self.logger.info(
            'Downloading data starting at %s',
            self.builder.get_gamecode())
        temp_data_stats = []
        end = end or self.build_end_game()
        self.logger.info('Downloading data ending at %s', end.get_gamecode())
        pool = TreadPool.BatchedThreadPool()
        counter = 0
        while counter < 1400 and self.builder <= end:
            url = self.builder.build_api_call()
            pool.create_thread(target=self.download_data, args=(url, counter))
            self.builder.increment_game()
            counter += 1
        pool.run_pool(max_threads=10)
        for i in range(counter):
            if i in self.data.keys():
                temp_data_stats.append(self.data[i])
        self.data = temp_data_stats
        self.logger.info('Downloaded %s games.', len(self.data))
        end = None
        return self


    def build_end_game(self):
        """ Return a builder for download_range() to end at. """
        self.logger.info('Building end game')
        end = GameApiBuilder().copy_other(self.builder)
        if self.builder._matchtype == '02':
            end.set_game_id(1274)
        elif self.builder._matchtype == '03':
            end.set_game(7)
        return end


    def parse_data(self):
        """ Parse the downloaded data. """
        self.logger.info('Parsing data.')
        self.ready_data = Parser.DataParser.parse_data(self.data)
        self.logger.info('Parsing complete.')
