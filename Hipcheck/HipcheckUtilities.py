DEFAULT_USERAGENT = (
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
    'Chrome/67.0.3396.79 Safari/537.36'
)


def convert_time(time_string):
    minute, second = time_string.split(':')
    return round(float(minute) + float(second) / 60, 2)
