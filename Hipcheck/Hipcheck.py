""" Hip Check Analytics Database Management System

Joins the different components of the Hip Check Analytics project into one
accessible engine.

Examples:
    dbms = dbms_engine.Hipcheck()
        or
    dbms = dbms_engine.Hipcheck('path/to/database.db')

    # Open database
    dbms.open_database('path/to/database.db')

    # New database
    dbms.new_database('path/to/database.db')

    # Download games 1-30 of the 2016-17 season and add it to the database
    start = acquirer.GameApiBuilder().set_season(2016).set_matchtype('season')\
        .set_game_id(1)
    end = acquirer.GameApiBuilder().set_season(2016).set_matchtype('season')\
        .set_game_id(30)
    dbms.fully_process()

    # fully_process() is equivilant too
    dbms.acquire_data()
    dbms.parse_data()
    dbms.append_data()

    # Query the database
    dbms.post_query('SELECT * FROM game_info')
"""
import datetime
import logging
import os
import pandas as pd
import sys
sys.path.append(os.path.abspath('.'))
import timeit
from Hipcheck import (
    file_finder,
    Acquirer as dat,
    Datastore
)
pd.options.display.width = 280

class Hipcheck:
    """ Engine for the Hipcheck Analytics project. """
    datastore = None
    acquirer = None

    def __init__(self, filename=None, logger=None):
        self.logger = logger or logging.getLogger(__name__)
        logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        handlers=[
            logging.FileHandler('Logs\\dbms.log'),
            logging.StreamHandler()
        ])
        self.logger.info('Initializing DBMS engine.')
        self.datastore = Datastore.HipcheckDatastore(filename, self.logger)

    def flush_data(self, args=None):
        """ Flush available data from acquirer and parser. """
        if self.acquirer is None:
            self.logger.info('No data to flush.')
            return
        self.logger.info('Flushing data.')
        self.acquirer.data = None
        self.acquirer.ready_data = None

    def fully_process(self, start, end):
        #import ipdb;ipdb.set_trace()
        """ Acquire, parse and append data to opened database. """
        self.logger.info('Processing data from %s - %s.', start, end)
        self.acquire_data(start, end)
        self.parse_data()
        self.append_to_database()

    def acquire_data(self, start, end):
        """ Execute acquirer with given start and end. """
        self.logger.info('Acquiring data.')
        start_time = timeit.default_timer()
        self.acquirer = dat.Acquirer(start)
        self.acquirer.download_range(end)
        end_time = timeit.default_timer()
        self.logger.info(
            'Completed data acquisition in %s\'s.', end_time - start_time
            )

    def parse_data(self, args=None):
        """ Execute parser with available acquirer data. """
        self.logger.info('Parsing data.')
        start_time = timeit.default_timer()
        self.acquirer.parse_data()
        end_time = timeit.default_timer()
        self.logger.info(
            'Completed data parsing in %s\'s.', end_time - start_time
            )

    def download_all(self):
        first_year = 1917
        current_year = datetime.datetime.now().year
        for year in range(first_year, current_year+1):
            self.download_year(year)

    def download_year(self, year):
        print('Downloading %s season.' % year)
        self.download_season(year)
        print('Downloading %s playoffs.' % year)
        self.download_playoffs(year)

    def download_season(self, year):
        start = dat.GameApiBuilder().set_season(year).set_matchtype('season').set_game_id(1)
        end = dat.GameApiBuilder().set_season(year).set_matchtype('season').set_game_id(1274)
        self.fully_process(start, end)

    def download_playoffs(self, year):
        for round in range(1, 5):
            self.download_round(year, round)

    def download_round(self, year, round):
        num_series = 2 ** (4 - round)
        for series in range(1, num_series):
            self.download_series(year, round, series)

    def download_series(self, year, round, series):
        start = dat.GameApiBuilder().set_season(year).set_matchtype('playoffs').set_round(round)\
            .set_series(series).set_game(1)
        end = dat.GameApiBuilder().set_season(year).set_matchtype('playoffs').set_round(round)\
            .set_series(series).set_game(7)
        self.fully_process(start, end)

    def current_db(self, args=None):
        """ Return the path to the currently opened database. """
        self.logger.info('Returning current database.')
        if self.datastore.database_path is None:
            self.logger.info('No database open.')
            return 'No Database Open.'
        return self.datastore.database_path

    # Possibly should move logic to Utilities or Datastore
    def list_databases(self, path='.\\Databases'):
        """ Return a list of databases found in path. """
        self.logger.info('Listing databases.')
        if path is None:
            self.logger.debug('Using default path.')
            path = '.\\Databases'
        path = path.strip()
        files = file_finder.list_files(path=path, extension='.db')
        return files

    def open_db(self, filename):
        """ Open the database found at filename. """
        self.datastore.open_database(filename)

    def new_database(self, filename):
        self.datastore.new_database(filename)

    def post_query(self, query):
        """ Return query result from opened database. """
        return self.datastore.post_query(query)

    def commit_data(self, args=None):
        """ Commit the data to the database. """
        self.datastore.commit_data()
        self.logger.info('Committed data to database.')

    def append_to_database(self, args=None):
        """ Append available data from parser to the opened database. """
        self.datastore.append_to_database(self.acquirer.ready_data)
