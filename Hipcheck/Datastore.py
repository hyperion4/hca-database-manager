import logging
import sqlite3
import numpy as np
import os.path
import pandas as pd
import timeit
import traceback

DATABASE_DIR = 'Databases\{}'
SELECT_ALL = 'select * from {}'


class NoDatabaseException(Exception):
    pass


class DataStoreError(Exception):
    pass


class HipcheckDatastore:
    database_path = None
    database_connection = None
    database_cursor = None

    def __init__(self, filename=None, logger=None):
        self.logger = logger or logging.getLogger(__name__)
        if filename:
            self.open_database(filename)

    def __getitem__(self, item):
        data = self.post_query(SELECT_ALL.format(item))
        if data is []:
            raise KeyError('Invalid table name')
        return data

    def open_database(self, filename, skip_lookup=False):
        """ Open the database found at filename. """
        self.logger.info('Opening database %s.', filename)
        filename = DATABASE_DIR.format(filename.strip())
        if not filename.endswith('.db'):
            filename += '.db'
        try:
            if not os.path.exists(filename) and not skip_lookup:
                raise NoDatabaseException('Database does not exist.')
            self.database_path = filename
            self.database_connection = sqlite3.connect(filename)
            self.database_cursor = self.database_connection.cursor()
            self.logger.info('Successfully opened database.')
        except Exception as e:
            print(traceback.print_exc())
            self.logger.info('Failed to opened database at path {}'.format(filename))

    def close_database(self):
        self.database_connection.close()

    def new_database(self, filename):
        """ Create a new database at path. """
        self.logger.info('Creating new database %s.', filename)
        self.open_database(filename, skip_lookup=True)
        self.database_cursor.execute(
            'SELECT name FROM sqlite_master WHERE type="table";'
        )
        # Easiest way to trim off a level of directories is to use os.path.dirname
        if self.database_cursor.fetchall() == []:
            script = os.path.dirname(
                os.path.dirname(os.path.realpath(__file__))
                ) + '\\Databases\\NHL_adminData.schema'
            with open(script) as sqlscript:
                self.database_cursor.executescript(sqlscript.read())
            self.logger.info('Database successfully created.')
        else:
            self.logger.info('Database with same path already exists.')

    def post_query(self, query):
        """ Return query result from opened database. """
        if self.database_connection is not None:
            start_time = timeit.default_timer()
            query = query.strip()
            answer = pd.read_sql_query(query, self.database_connection)
            end_time = timeit.default_timer()
            self.logger.info('Query returned in %s\'s.', end_time - start_time)
            return answer
        print('No database connection.')
        self.logger.info('No database connection.')
        return []

    def commit_data(self):
        """ Commit the data to the database. """
        self.database_connection.commit()
        # self.logger.info('Committed data to database.')

    def append_to_database(self, datasets):
        """ Append available data from parser to the opened database. """
        self.logger.info('Appending data to database %s.', self.database_path)
        start_time = timeit.default_timer()
        dataset_names = [
            'game_info',
            'game_events',
            'player_data',
            'team_data',
            'player_stats',
            'goalie_stats',
            'team_stats',
            'scratches',
            'officials',
            'three_stars'
        ]
        for name in dataset_names:
            self.append_by_row(datasets, name)
        self.database_connection.commit()  # Should this be happening?
        end_time = timeit.default_timer()
        self.logger.info('Data appended in %s\'s.', end_time - start_time)

    def append_by_row(self, datasets, dataset_name):
        """ Insert or ignore each row of the dataset into the database. """
        self.logger.info('Inserting data into %s.', dataset_name)
        try:
            columns = list(datasets[dataset_name].iloc[0].index)
            for _, row in datasets[dataset_name].iterrows():
                values = list(row.replace(np.nan, 'None').values)
                data_repr = [value.__repr__() for value in values]
                query = 'INSERT OR IGNORE INTO ' + dataset_name + '(' + \
                        ','.join(columns) + ') VALUES(' + ','.join(data_repr) + ')'
                self.logger.debug('Appending row with query, %s.', query)
                self.database_cursor.execute(query)
        except Exception as e:
            self.logger.debug('append_by_row: %s' % e)
            print(e)
