""" Data parser to convert JSON from the NHL into pandas

Parses out all relevent information from JSON objects received from NHLs public
api. The information is returned as a dictionary of pandas dataframe for
usability and its similarities in structure to SQL.

Example:
    parsed_data = data_parser.DataParser.parse_data(data)
    print(parsed_data.keys())
    ['player_stats', 'goalie_stats', 'game_info', 'player_data', 'team_data',
    'game_events', 'team_stats', 'scratches', 'officials', 'three_stars']
"""
import logging
import pandas as pd

from Hipcheck import HipcheckUtilities

GAMEID_PATH = 'gameData game pk'

GAME_INFO_PATHS = {
    'game_id': GAMEID_PATH,
    'season': 'gameData game season',
    'match_type': 'gameData game type',
    'start': 'gameData datetime dateTime',
    'end': 'gameData datetime endDateTime',
    'away_team': 'gameData teams away id',
    'home_team': 'gameData teams home id',
    'away_coach': 'liveData boxscore teams away coaches 0 person fullName',
    'home_coach': 'liveData boxscore teams home coaches 0 person fullName'
}

PLAYER_IDS_PATH = 'gameData players'

PLAYER_DATA_PATHS = {
    'player_id': 'gameData players {0} id',
    'player_name': 'gameData players {0} fullName',
    'current_team': 'gameData players {0} currentTeam name',
    'number': 'gameData players {0} primaryNumber',
    'birthday': 'gameData players {0} birthDate',
    'birth_city': 'gameData players {0} birthCity',
    'birth_country': 'gameData players {0} birthCountry',
    'nationality': 'gameData players {0} nationality',
    'height': 'gameData players {0} height',
    'weight': 'gameData players {0} weight',
    'shoots_catches': 'gameData players {0} shootsCatches',
    'position': 'gameData players {0} primaryPosition name'
}

PLAYER_DATA_PROCESS = {
    'height': (lambda x: x[0:-1])
}

TEAM_DATA_PATHS = {
    'team_id': 'gameData teams {0} id',
    'team_name': 'gameData teams {0} teamName',
    'venue': 'gameData teams {0} venue name',
    'city': 'gameData teams {0} locationName',
    'time_zone': 'gameData teams {0} venue timeZone tz',
    'abbreviation': 'gameData teams {0} abbreviation',
    'year_one': 'gameData teams {0} firstYearOfPlay',
    'division': 'gameData teams {0} division name',
    'conference': 'gameData teams {0} conference name',
}

GAME_EVENT_IDS = 'liveData plays allPlays'

GAME_EVENT_PATHS = {
    'game_id': GAMEID_PATH,
    'event_id': 'liveData plays allPlays {0} about eventId',
    'primary_player': 'liveData plays allPlays {0} players 0 player id',
    'secondary_player': 'liveData plays allPlays {0} players 1 player id',
    'event': 'liveData plays allPlays {0} result event',
    'description': 'liveData plays allPlays {0} result description',
    'period': 'liveData plays allPlays {0} about period',
    'periodtype': 'liveData plays allPlays {0} about periodType',
    'period_time': 'liveData plays allPlays {0} about periodTime',
    'dateTime': 'liveData plays allPlays {0} about dateTime',
    'away_score': 'liveData plays allPlays {0} about goals away',
    'home_score': 'liveData plays allPlays {0} about goals home',
    'x': 'liveData plays allPlays {0} coordinates x',
    'y': 'liveData plays allPlays {0} coordinates y'
}

PLAYER_STAT_IDS = 'liveData boxscore teams {0} players'

PLAYER_STATS_PATHS = {
    'player_id': 'liveData boxscore teams {0} players {1} person id',
    'game_id': GAMEID_PATH,
    'time_on_ice': 'liveData boxscore teams {0} players {1} stats skaterStats timeOnIce',
    'goals': 'liveData boxscore teams {0} players {1} stats skaterStats goals',
    'assists': 'liveData boxscore teams {0} players {1} stats skaterStats assists',
    'shots': 'liveData boxscore teams {0} players {1} stats skaterStats shots',
    'hits': 'liveData boxscore teams {0} players {1} stats skaterStats hits',
    'pp_goals': 'liveData boxscore teams {0} players {1} stats skaterStats powerPlayGoals',
    'pp_assists': 'liveData boxscore teams {0} players {1} stats skaterStats powerPlayAssists',
    'penalty_minutes': 'liveData boxscore teams {0} players {1} stats skaterStats penaltyMinutes',
    'faceoffs_won': 'liveData boxscore teams {0} players {1} stats skaterStats faceOffWins',
    'faceoffs_taken': 'liveData boxscore teams {0} players {1} stats skaterStats faceoffTaken',
    'takeaways': 'liveData boxscore teams {0} players {1} stats skaterStats takeaways',
    'giveaways': 'liveData boxscore teams {0} players {1} stats skaterStats giveaways',
    'sh_goals': 'liveData boxscore teams {0} players {1} stats skaterStats shortHandedGoals',
    'sh_assists': 'liveData boxscore teams {0} players {1} stats skaterStats shortHandedAssists',
    'blocked': 'liveData boxscore teams {0} players {1} stats skaterStats blocked',
    'plus_minus': 'liveData boxscore teams {0} players {1} stats skaterStats plusMinus',
    'even_time_on_ice': 'liveData boxscore teams {0} players {1} stats skaterStats evenTimeOnIce',
    'pp_time_on_ice': 'liveData boxscore teams {0} players {1} stats skaterStats powerPlayTimeOnIce',
    'sh_time_on_ice': 'liveData boxscore teams {0} players {1} stats skaterStats shortHandedTimeOnIce'
}

PLAYER_STATS_PROCESS = {
    'time_on_ice': HipcheckUtilities.convert_time,
    'even_time_on_ice': HipcheckUtilities.convert_time,
    'pp_time_on_ice': HipcheckUtilities.convert_time,
    'sh_time_on_ice': HipcheckUtilities.convert_time
}

GOALIE_STATS_PATHS = {
    'player_id': 'liveData boxscore teams {0} players {1} person id',
    'game_id': GAMEID_PATH,
    'assists': 'liveData boxscore teams {0} players {1} stats goalieStats assists',
    'decision': 'liveData boxscore teams {0} players {1} stats goalieStats decision',
    'even_saves': 'liveData boxscore teams {0} players {1} stats goalieStats evenSaves',
    'even_shots_against': 'liveData boxscore teams {0} players {1} stats goalieStats evenShotsAgainst',
    'goals': 'liveData boxscore teams {0} players {1} stats goalieStats goals',
    'pim': 'liveData boxscore teams {0} players {1} stats goalieStats pim',
    'pp_saves': 'liveData boxscore teams {0} players {1} stats goalieStats powerPlaySaves',
    'pp_shots': 'liveData boxscore teams {0} players {1} stats goalieStats powerPlayShotsAgainst',
    'saves': 'liveData boxscore teams {0} players {1} stats goalieStats saves',
    'shots': 'liveData boxscore teams {0} players {1} stats goalieStats shots',
    'sh_saves': 'liveData boxscore teams {0} players {1} stats goalieStats shortHandedSaves',
    'sh_shots': 'liveData boxscore teams {0} players {1} stats goalieStats shortHandedShotsAgainst',
    'time_on_ice': 'liveData boxscore teams {0} players {1} stats goalieStats timeOnIce',
}

TEAM_STATS_PATHS = {
    'team_id': 'liveData boxscore teams {0} team id',
    'game_id': GAMEID_PATH,
    'blocked': 'liveData boxscore teams {0} teamStats teamSkaterStats blocked',
    'faceoffpct': 'liveData boxscore teams {0} teamStats teamSkaterStats faceOffWinPercentage',
    'giveaways': 'liveData boxscore teams {0} teamStats teamSkaterStats giveaways',
    'takeaways': 'liveData boxscore teams {0} teamStats teamSkaterStats takeaways',
    'goals': 'liveData boxscore teams {0} teamStats teamSkaterStats goals',
    'hits': 'liveData boxscore teams {0} teamStats teamSkaterStats hits',
    'pim': 'liveData boxscore teams {0} teamStats teamSkaterStats pim',
    'pp_goals': 'liveData boxscore teams {0} teamStats teamSkaterStats powerPlayGoals',
    'pp_oppertunities': 'liveData boxscore teams {0} teamStats teamSkaterStats powerPlayOpportunities',
    'pppct': 'liveData boxscore teams {0} teamStats teamSkaterStats powerPlayPercentage',
    'shots': 'liveData boxscore teams {0} teamStats teamSkaterStats shots',
}

OFFICIALS_IDS = 'liveData boxscore officials'

OFFICIALS_PATHS = {
    'game_id': GAMEID_PATH,
    'ref_name': 'liveData boxscore officials {0} official fullName',
    'official_type': 'liveData boxscore officials {0} officialType'
}
'''

'''
THREE_STARS_PATHS = {
    'game_id': GAMEID_PATH,
    'first_star': 'liveData decisions firstStar id',
    'second_star': 'liveData decisions secondStar id',
    'third_star': 'liveData decisions thirdStar id'
}

def deep_get(_dict, _path, process=None):
    _path = _path.split(' ')
    try:
        for key in _path:
            if isinstance(_dict, list):
                _dict = _dict[int(key)]
            else:
                _dict = _dict[key]
    except KeyError:
        #print('KeyError: Invalid path')
        return None
    except IndexError:
        #print('IndexError: Invalid path')
        return None
    try:
        if process:
            return process(_dict)
    except:
        #print('Lambda error: could not convert value')
        pass
    return _dict


class GameParser:
    """
    Parse a JSON payload of NHL data into a dictionary of pandas dataframes.
    """
    def __init__(self, data, logger=None):
        self.logger = logger or logging.getLogger(__name__)
        self.data = data


    def get_data(self, path_group, process_group={}):
        """ Parse out game information from a game. """
        data = {}
        for key, path in path_group.items():
            process = None
            if key in process_group.keys():
                process = process_group[key]
            value = deep_get(self.data, path, process)
            if value or value == 0:
                data[key] = value
        return pd.DataFrame(data, index=[0])


    def get_data_range(self, ids, paths, process_group={}):
        data = []
        for id in ids:
            formatted_paths = {}
            for key, value in paths.items():
                formatted_paths[key] = value.format(id)
            data.append(self.get_data(formatted_paths, process_group))
        if len(data):
            return pd.concat(data)


    def get_player_stats(self):
        """ Parse out player statistics from a game. """
        player_stats = []
        goalie_stats = []
        ah_keys = ['home', 'away']
        for _key in ah_keys:
            player_ids = deep_get(self.data, PLAYER_STAT_IDS.format(_key)).keys()
            for id in player_ids:
                player_type = list(deep_get(self.data, 'liveData boxscore teams {0} players {1} stats'.format(_key, id)).keys())
                if player_type:
                    player_type = player_type[0]
                if player_type == 'skaterStats':
                    formatted_paths = {}
                    for key, value in PLAYER_STATS_PATHS.items():
                        formatted_paths[key] = value.format(_key, id)
                    player_stats.append(self.get_data(formatted_paths))
                elif player_type == 'goalieStats':
                    formatted_paths = {}
                    for key, value in GOALIE_STATS_PATHS.items():
                        formatted_paths[key] = value.format(_key, id)
                    goalie_stats.append(self.get_data(formatted_paths))
        self.logger.debug('Finished parsing player stats.')
        if len(player_stats) > 1 and len(goalie_stats) > 1:
            return pd.concat(player_stats), pd.concat(goalie_stats)
        return None, None


    def get_scratches(self):
        """ Parse out scratches from a game. """
        scratches = []
        ah_keys = ['home', 'away']
        game_gid = self.data['gameData']['game']['pk']
        for key in ah_keys:
            half_data = self.data['liveData']['boxscore']['teams'][key]\
                ['scratches']
            for i in half_data:
                self.logger.debug('Parsing scratch: %s', i)
                self.logger.debug('From game: %s', game_gid)
                scratched = {}
                scratched['player_id'] = i
                scratched['game_id'] = game_gid
                scratches.append(scratched)
        self.logger.debug('Finished parsing scratches.')
        return pd.DataFrame(scratches)


    def fully_parse(self):
        """ Parse out all data and store it. """
        self.logger.debug('Fully parsing data.')
        ready_data = {}
        ready_data['player_stats'], ready_data['goalie_stats'] =\
            self.get_player_stats()
        ready_data['game_info'] = self.get_data(GAME_INFO_PATHS)
        player_ids = deep_get(self.data, PLAYER_IDS_PATH)
        ready_data['player_data'] = self.get_data_range(player_ids, PLAYER_DATA_PATHS, PLAYER_DATA_PROCESS)
        ready_data['team_data'] = self.get_data_range(['home', 'away'], TEAM_DATA_PATHS)
        play_ids = range(0, len(deep_get(self.data, GAME_EVENT_IDS)))
        ready_data['game_events'] = self.get_data_range(play_ids, GAME_EVENT_PATHS)
        ready_data['team_stats'] = self.get_data_range(['home', 'away'], TEAM_STATS_PATHS)

        ready_data['scratches'] = self.get_scratches()
        ref_ids = range(len(deep_get(self.data, OFFICIALS_IDS)))
        ready_data['officials'] = self.get_data_range(ref_ids, OFFICIALS_PATHS)
        ready_data['three_stars'] = self.get_data(THREE_STARS_PATHS)
        self.logger.debug('Finished parsing.')
        return ready_data

    def convert_time(time_string):
        minute, second = time_string.split(':')
        return round(float(minute) + float(second) / 60, 2)

class DataParser:
    """ Returns parsed data from a set of games using GameParser. """
    def parse_data(data):
        """ Parse data out from a set of games. """
        logger = logging.getLogger(__name__)
        logger.info('Parsing over range of data.')
        ready_data = {
            'game_info': None,
            'goalie_stats': None,
            'player_stats': None,
            'player_data': None,
            'team_data': None,
            'game_events': None,
            'team_stats': None,
            'scratches': None,
            'officials': None,
            'three_stars': None
            }
        for dataset in data:
            parser = GameParser(dataset)
            parsed_data = parser.fully_parse()
            for key in ready_data.keys():
                if ready_data[key] is not None or parsed_data[key] is not None:
                    ready_data[key] = pd.concat([ready_data[key], parsed_data[key]])
        logger.info('Finished parsing data range.')
        return ready_data
