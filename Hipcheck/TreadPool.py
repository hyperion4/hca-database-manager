""" Thread pool class for dealing with arbitrary numbers of threads.

Currently provides a batching based solution for dealing with threads.
Simply add the threads to the pool and execute the pool.

Example:
    pool = thread_pool.BatchedThreadPool()
    pool.create_thread(foo, 3)
    pool.create_thread(foo, 4)
    pool.run_pool()
"""
import threading
import logging

class ThreadPool:
    """ Deal with executing an arbitrary numbers of threads. """
    running_threads = []
    waiting_threads = []
    def __init__(self, logger):
        self.logger = logger or logging.getLogger(__name__)

    def create_thread(self, target, args):
        """ Initialize and add thread object to pool. """
        self.waiting_threads.append(threading.Thread(target=target, args=args))

    def add_thread(self, thread):
        """ Add preinitialized thread object to pool. """
        self.waiting_threads.append(thread)

class BatchedThreadPool(ThreadPool):
    """ Executes thread pool based on batching premise. """
    def __init__(self, logger=None):
        super(BatchedThreadPool, self).__init__(logger)
        self.logger.info('Batch based thread pool created.')

    def run_pool(self, max_threads):
        """ Execute the pool in batches until all threads returned. """
        self.logger.info(
            'Executing thread pool with batch size ' + str(max_threads)
            )
        while self.waiting_threads:
            self.logger.debug('Beggining batch.')
            for _ in range(max_threads):
                if self.waiting_threads:
                    thread = self.waiting_threads.pop()
                    thread.start()
                    self.running_threads.append(thread)
            self.logger.debug('Joining batch.')
            for thread in self.running_threads:
                thread.join()
            self.running_threads = []
