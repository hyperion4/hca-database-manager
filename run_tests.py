""" Stand alone test runner for the Hip Check Analytics project

Provides a seamless way for potential users to run all unit tests for the
project and read a quick report. Main project development takes place in visual
studios and so the runner mimics its functionality of finding all test files by
looking for filenames test*. Any new tests can be added to a unittest file
within the Tests directory with filename test* and  test_runner will
automatically add it to the suite.

Examples:
    $ run_tests.py
    $ python run_tests.py
"""
#!/usr/bin/env python
import unittest
import sys
import os
from Hipcheck import file_finder
sys.path.append(os.path.abspath('.') + '\\Tests')

class TestRunner:
    """ Find all files test* in .\\Tests and runs their test cases. """
    test_modules = []
    results = ''
    def find_tests(self):
        """ Find all test files with name test* in .\\Tests. """
        paths = file_finder.list_files('.\\Tests', '.py')
        # Trim off path
        test_modules = [path.split('\\')[-1] for path in paths]
        # Filter non test files
        test_modules = [module for module
                        in test_modules
                        if str.startswith(module, 'test')]
        # Trim off .py
        self.test_modules = [module.split('.')[0] for module in test_modules]

    def run_tests(self):
        """ Run all the tests using the unittest framework. """
        loader = unittest.TestLoader()
        suite = unittest.TestSuite()
        for module in self.test_modules:
            module = __import__(module)
            suite.addTests(loader.loadTestsFromModule(module))
        runner = unittest.TextTestRunner(verbosity=3)
        self.results = runner.run(suite)

    def show_results(self):
        """ Print out the results string and wait for press of enter. """
        print(self.results)
        input('Press enter to exit.')

    def start_run(self):
        """ Execute each section of the testing process in proper order. """
        self.find_tests()
        self.run_tests()
        self.show_results()

if __name__ == '__main__':
    TestRunner().start_run()
