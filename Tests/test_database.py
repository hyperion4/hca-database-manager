import sys
import os
import unittest
sys.path.append(os.path.abspath('..'))
from Analytics.Database import Analytics


class TestDatabase(unittest.TestCase):

    def setUp(self):
        self.db = Analytics('Test_dbs\\test_3')

    def tearDown(self):
        self.db.close_database()
        self.db = None

    def test_contexts(self):
        expected_data = {
                'abbreviation': ['TOR'],
                'city': ['Toronto'],
                'conference': ['Eastern'],
                'division': ['Atlantic'],
                'team_id': [10],
                'team_name': ['Maple Leafs'],
                'time_zone': ['EDT'],
                'venue': ['Air Canada Centre'],
                'year_one': [1926]
        }
        data = self.db.contexts['team_data'][10]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'blocked': [9],
                'faceoffpct': [51.5],
                'game_id': [2016020001],
                'giveaways': [6],
                'goals': [4],
                'hits': [21],
                'pim': [11],
                'pp_goals': [0],
                'pp_oppertunities': [4.0],
                'pppct': [0.0],
                'shots': [38],
                'takeaways': [8],
                'team_id': [10]
        }
        data = self.db.contexts['team_stats'][10]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'birth_city': ['Edmonton'],
                'birth_country': ['CAN'],
                'birthday': ['1985-04-10'],
                'current_team': ['Los Angeles Kings'],
                'height': ["6' 4"],
                'nationality': ['CAN'],
                'number': [3],
                'player_id': [8470602],
                'player_name': ['Dion Phaneuf'],
                'position': ['Defenseman'],
                'shoots_catches': ['L'],
                'weight': [225]
        }
        data = self.db.contexts['player_data'][8470602]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'assists': [0],
                'blocked': [5],
                'even_time_on_ice': ['17:13'],
                'faceoffs_taken': [0],
                'faceoffs_won': [0],
                'game_id': [2016020001],
                'giveaways': [0],
                'goals': [0],
                'hits': [3],
                'penalty_minutes': [2],
                'player_id': [8470602],
                'plus_minus': [-1],
                'pp_assists': [0],
                'pp_goals': [0],
                'pp_time_on_ice': ['1:07'],
                'sh_assists': [0],
                'sh_goals': [0],
                'sh_time_on_ice': ['3:46'],
                'shots': [0],
                'takeaways': [0],
                'time_on_ice': ['22:06']
        }
        data = self.db.contexts['player_stats'][8470602]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'game_id': [2016020001],
                'player_id': [8476477]
        }
        data = self.db.contexts['scratches'][8476477]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'away_coach': ['Mike Babcock'],
                'away_team': [10],
                'end': ['2016-10-13T01:56:40Z'],
                'game_id': [2016020001],
                'home_coach': ['Guy Boucher'],
                'home_team': [9],
                'match_type': ['R'],
                'season': [20162017],
                'start': ['2016-10-12T23:00:00Z']
        }
        data = self.db.contexts['game_info'][2016020001]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'game_id': [2016020002, 2016020002, 2016020002, 2016020002],
                'official_type': ['Referee', 'Referee', 'Linesman', 'Linesman'],
                'ref_name': ['Jon McIsaac', 'Ian Walsh', 'Ryan Galloway', 'Devin Berg']
        }
        data = self.db.contexts['officials'][2016020002]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'away_score': [0, 0, 0, 0, 0],
                'dateTime': ['2016-10-12T22:14:14Z',
                             '2016-10-13T00:07:08Z',
                             '2016-10-13T00:12:27Z',
                             '2016-10-13T00:12:27Z',
                             '2016-10-13T00:12:41Z'],
                'description': ['Game Scheduled',
                                'Period Ready',
                                'Period Start',
                                'Paul Stastny faceoff won against Jonathan Toews',
                                'Paul Stastny hit Duncan Keith'],
                'event': ['Game Scheduled', 'Period Ready', 'Period Start', 'Faceoff', 'Hit'],
                'event_id': [1, 2, 5, 6, 51],
                'game_id': [2016020002, 2016020002, 2016020002, 2016020002, 2016020002],
                'home_score': [0, 0, 0, 0, 0],
                'period': [1, 1, 1, 1, 1],
                'period_time': ['00:00', '00:00', '00:00', '00:00', '00:11'],
                'periodtype': ['REGULAR', 'REGULAR', 'REGULAR', 'REGULAR', 'REGULAR'],
                'primary_player': ['None', 'None', 'None', 8471669, 8471669],
                'secondary_player': ['None', 'None', 'None', 8473604, 8470281],
                'x': ['None', 'None', 'None', 0.0, 88.0],
                'y': ['None', 'None', 'None', 0.0, -37.0]
        }
        data = self.db.contexts['game_events'][2016020002]
        self.assertEqual(data.head().to_dict('list'), expected_data)

        expected_data = {
                'assists': [0],
                'decision': ['W'],
                'even_saves': [27],
                'even_shots_against': [31],
                'game_id': [2016020001],
                'goals': [0],
                'pim': [0],
                'player_id': [8467950],
                'pp_saves': [7],
                'pp_shots': [7],
                'saves': [34],
                'sh_saves': [0],
                'sh_shots': [0],
                'shots': [38],
                'time_on_ice': ['60:37']
        }
        data = self.db.contexts['goalie_stats'][8467950]
        self.assertEqual(data.to_dict('list'), expected_data)

        expected_data = {
                'first_star': ['8475765'],
                'game_id': [2016020002],
                'second_star': ['8471669'],
                'third_star': ['8477451']
        }
        data = self.db.contexts['three_stars'][2016020002]
        self.assertEqual(data.to_dict('list'), expected_data)
