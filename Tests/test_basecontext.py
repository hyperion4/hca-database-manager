import unittest
import sys
import os
sys.path.append(os.path.abspath('..'))
from Analytics.Database import Analytics
from Analytics.BaseContext import BaseContext


class TestBaseContext(unittest.TestCase):

    def setUp(self):
        db = Analytics('Test_dbs\\test_3')
        self.context = BaseContext(db['team_data'], 'team_data')
        db.close_database()

    def tearDown(self):
        self.context = None

    def test_getitem(self):
        expected_data = {
                'abbreviation': ['TOR'],
                'city': ['Toronto'],
                'conference': ['Eastern'],
                'division': ['Atlantic'],
                'team_id': [10],
                'team_name': ['Maple Leafs'],
                'time_zone': ['EDT'],
                'venue': ['Air Canada Centre'],
                'year_one': [1926]
        }
        data = self.context[10]
        self.assertEqual(data.to_dict('list'), expected_data)
