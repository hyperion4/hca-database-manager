import json
import unittest
import pandas as pd
import sys
import os
sys.path.append(os.path.abspath('..'))
from Hipcheck import Hipcheck
from Hipcheck import file_finder
from Hipcheck import Acquirer as dat

class Test_fileFinder(unittest.TestCase):
    def test_fileFinder(self):
        files = file_finder.list_files()
        self.assertGreater(len(files), 4)

    def test_find_databases(self):
        databases = file_finder.list_files('Databases\Test_dbs', extension='.db')
        self.assertEqual(
            databases, [
                'Databases\\Test_dbs\\test.db',
                'Databases\\Test_dbs\\test_2.db',
                'Databases\\Test_dbs\\test_3.db'
            ]
        )

class Test_DBMS_functions(unittest.TestCase):
    def setUp(self):
        self.engine = Hipcheck.Hipcheck()

    def tearDown(self):
        self.engine = None
        try:
            os.remove('Databases\\Test_dbs\\test_4.db')
        except OSError:
            pass

    def test_find_databases(self):
        path = 'Databases\Test_dbs'
        databases = self.engine.list_databases(path=path)
        self.assertEqual(
            databases, [
                'Databases\\Test_dbs\\test.db',
                'Databases\\Test_dbs\\test_2.db',
                'Databases\\Test_dbs\\test_3.db'
            ]
        )

    def test_find_databases_empty(self):
        path = '.'
        databases = self.engine.list_databases(path)
        self.assertEqual(databases, [])

    def test_open_db(self):
        excepted_data = {'abbreviation': ['TOR', 'WPG'],
                         'city': ['Toronto', 'Winnipeg'],
                         'conference': ['Atlantic', 'Central'],
                         'division': ['Eastern', 'Western'],
                         'team_id': [10, 52],
                         'team_name': ['Maple Leafs\t', 'Jets'],
                         'time_zone': ['EST', 'CST'],
                         'venue': ['Air Canada Centre', 'Bell MTS Place'],
                         'year_one': [1926, 2011]}
        path = 'Test_dbs\\test'
        self.engine.open_db(path)
        sql_command = """SELECT * FROM teams;"""
        team_info = pd.read_sql_query(sql_command, self.engine.datastore.database_connection)
        self.assertEqual(team_info.to_dict('list'), excepted_data)

    def test_new_database(self):
        try:
            os.remove('Test_dbs\\test_4')
        except OSError:
            pass
        self.engine.new_database('Test_dbs\\test_4')
        tables = self.engine.datastore.database_cursor.execute(
            'SELECT name FROM sqlite_master WHERE type="table";'
        ).fetchall()
        self.assertEquals(len(tables), 10)

    def test_acquire_data(self):
        start = dat.GameApiBuilder().set_season(2016).set_matchtype('season').set_game_id(238)
        end = dat.GameApiBuilder().set_season(2016).set_matchtype('season').set_game_id(239)
        self.engine.acquire_data(start, end)
        self.assertEqual(len(self.engine.acquirer.data), 2)

    def test_acquire_parse_data(self):
        data = []
        url = '.\Tests\\2016020238.json'
        with open(url, 'r') as f:
            data.append(json.load(f))
        url = '.\Tests\\2016020239.json'
        with open(url, 'r') as f:
            data.append(json.load(f))
        self.engine.acquirer = dat.Acquirer(None)
        self.engine.acquirer.data = data
        self.engine.parse_data()
        self.assertEqual(len(self.engine.acquirer.ready_data['game_info']), 2)


class Test_DBMS_process(unittest.TestCase):
    def setUp(self):
        self.engine = Hipcheck.Hipcheck()
        self.engine.open_db('Test_dbs\\test_3')
        tables = self.engine.post_query("SELECT name FROM sqlite_master WHERE type='table';")['name'].values
        for table in tables:
            self.engine.datastore.database_cursor.execute('DELETE FROM ' + table)

    def tearDown(self):
        self.engine.datastore.close_database()
        self.engine = None

    def test_process_data(self):
        start = dat.GameApiBuilder().set_season(2016).set_matchtype('season').set_game_id(1)
        end = dat.GameApiBuilder().set_season(2016).set_matchtype('season').set_game_id(10)
        self.engine.fully_process(start, end)
        self.assertEqual(len(self.engine.post_query('SELECT * FROM player_data')), 435)


class Test_DBMS_append(unittest.TestCase):
    def setUp(self):
        data = []
        url = '.\Tests\\2016020238.json'
        with open(url, 'r') as f:
            data.append(json.load(f))
        url = '.\Tests\\2016020239.json'
        with open(url, 'r') as f:
            data.append(json.load(f))
        self.engine = Hipcheck.Hipcheck()
        self.engine.open_db('Test_dbs\\test_3')
        tables = self.engine.post_query("SELECT name FROM sqlite_master WHERE type='table';")['name'].values
        for table in tables:
            self.engine.datastore.database_cursor.execute('DELETE FROM ' + table)
        self.engine.acquirer = dat.Acquirer(None)
        self.engine.acquirer.data = data
        self.engine.parse_data()
        self.engine.append_to_database()

    def tearDown(self):
        self.engine.datastore.close_database()
        self.engine = None

    def test_appended_gameinfo(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM game_info')), 2)

    def test_appended_playerdata(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM player_data')), 88)

    def test_appended_teamdata(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM team_data')), 4)

    def test_appended_playerstats(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM player_stats')), 72)

    def test_appended_teamstats(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM team_stats')), 4)

    def test_appended_gameevents(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM game_events')), 616)

    def test_appended_scratches(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM scratches')), 11)

    def test_appended_officials(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM officials')), 8)

    def test_appended_goaliestats(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM goalie_stats')), 5)

    def test_appended_threestars(self):
        self.assertEqual(len(self.engine.post_query('SELECT * FROM three_stars')), 2)

if __name__ == '__main__':
    unittest.main()
