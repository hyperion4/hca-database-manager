import unittest
import sys
import os
sys.path.append(os.path.abspath('..'))
from Hipcheck import Acquirer

class API_Builder_Tests(unittest.TestCase):
    def setUp(self):
        self.builder = Acquirer.GameApiBuilder()

    def tearDown(self):
        self.builder = None

    def test_default_code(self):
        self.assertEqual('0000000000', self.builder.get_gamecode())
    
    def test_set_code(self):
        self.builder.set_gamecode(2016020236)
        self.assertEqual('2016020236', self.builder.get_gamecode())

    def test_set_season(self):
        self.builder.set_season(2016)
        self.assertEqual('2016000000', self.builder.get_gamecode())

    def test_set_matchtype(self):
        self.builder.set_matchtype('playoffs')
        self.assertEqual('0000030000', self.builder.get_gamecode())

    def test_set_gameID_regular(self):
        self.builder.set_matchtype('season').set_game_id(238)
        self.assertEqual('0000020238', self.builder.get_gamecode())

    def test_set_gameID_playoff(self):
        self.builder.set_matchtype('playoffs').set_game_id(238)
        self.assertEqual('0000030000', self.builder.get_gamecode())

    def test_set_round_regular(self):
        self.builder.set_matchtype('season').set_round(3)
        self.assertEqual('0000020000', self.builder.get_gamecode())

    def test_set_round_playoff(self):
        self.builder.set_matchtype('playoffs').set_round(3)
        self.assertEqual('0000030300', self.builder.get_gamecode())

    def test_set_series_regular(self):
        self.builder.set_matchtype('season').set_series(4)
        self.assertEqual('0000020000', self.builder.get_gamecode())

    def test_set_series_playoff(self):
        self.builder.set_matchtype('playoffs').set_series(4)
        self.assertEqual('0000030040', self.builder.get_gamecode())

    def test_set_gamenumber_regular(self):
        self.builder.set_matchtype('season').set_game(7)
        self.assertEqual('0000020000', self.builder.get_gamecode())

    def test_set_gamenumber_playoff(self):
        self.builder.set_matchtype('playoffs').set_game(7)
        self.assertEqual('0000030007', self.builder.get_gamecode())

    def test_build_api_call_regular(self):
        self.builder.set_season(2016).set_matchtype('season').set_game_id(238)
        self.assertEqual('http://statsapi.web.nhl.com/api/v1/game/2016020238/feed/live', self.builder.build_api_call())

    def test_build_api_call_preseason(self):
        self.builder.set_season(2016).set_matchtype('preseason').set_game_id(238)
        self.assertEqual('http://statsapi.web.nhl.com/api/v1/game/2016010238/feed/live', self.builder.build_api_call())

    def test_build_api_call_playoff(self):
        self.builder.set_season(2016).set_matchtype('playoffs').set_round(3).set_series(1).set_game(7)
        self.assertEqual('http://statsapi.web.nhl.com/api/v1/game/2016030317/feed/live', self.builder.build_api_call())


if __name__ == '__main__':
    unittest.main()
