import unittest
import sys
import os
sys.path.append(os.path.abspath('..'))
from Hipcheck import Acquirer as dat

class Acquirer_class_tests(unittest.TestCase):
    def setUp(self):
        self.builder = dat.GameApiBuilder()
        self.builder.set_season(2016).set_matchtype('season').set_game_id(238)
        self.acquirer = dat.Acquirer(self.builder)

    def tearDown(self):
        self.builder = None
        self.acquirer = None

    def test_get_api(self):
        self.assertEqual('http://statsapi.web.nhl.com/api/v1/game/2016020238/feed/live', self.acquirer.get_api())

    def test_download_data(self):
        self.acquirer.download_data()
        self.assertEqual(self.acquirer.status, 202)

    def test_download_data_bad_api(self):
        self.builder.set_matchtype('preseason')
        self.acquirer.download_data()
        self.assertEqual(self.acquirer.status, 404)

    def test_download_range_length(self):
        end = dat.GameApiBuilder().set_season(2016).set_matchtype('season').set_game_id(240)
        self.acquirer.download_range(end)
        self.assertEqual(len(self.acquirer.data), 3)

    def test_download_playoff_series(self):
        self.builder.set_season(2016).set_matchtype('playoffs').set_round(1).set_series(1).set_game(1)
        self.acquirer.download_range()
        self.assertEqual(len(self.acquirer.data), 6)

    def test_download_data(self):
        end = dat.GameApiBuilder().set_season(2016).set_matchtype('season').set_game_id(239)
        self.acquirer.download_range(end)
        self.assertEqual(self.acquirer.data[0]['gamePk'], 2016020238)

    def test_download_data_second(self):
        end = dat.GameApiBuilder().set_season(2016).set_matchtype('season').set_game_id(239)
        self.acquirer.download_range(end)
        self.assertEqual(self.acquirer.data[1]['gamePk'], 2016020239)

if __name__ == '__main__':
    unittest.main()
