import unittest
import json
import numpy as np
import pandas as pd
import sys
import os
sys.path.append(os.path.abspath('..'))
from Hipcheck import Parser
from Hipcheck import Acquirer as dat

class GameParserTests(unittest.TestCase):
    def setUp(self):
        url = '.\Tests\\2016020237_testdata.json'
        with open(url, 'r') as f:
            self.data = json.load(f)
        self.parser = Parser.GameParser(self.data)

    def tearDown(self):
        self.data = None
        self.parser = None

    def test_parse_game_info(self):
        expected_info = {'away_coach': ['Glen Gulutzan'],
                             'away_team': [20],
                             'end': ['2016-11-16T03:40:44Z'],
                             'game_id': [2016020237],
                             'home_coach': ['Bruce Boudreau'],
                             'home_team': [30],
                             'match_type': ['R'],
                             'season': ['20162017'],
                             'start': ['2016-11-16T01:00:00Z']}
        game_info = self.parser.get_data(Parser.GAME_INFO_PATHS)
        self.assertEqual(game_info.to_dict('list'), expected_info)

    def test_parse_player_data_firstplayer(self):
        expected_info = {'birth_city': 'Toronto',
                             'birth_country': 'CAN',
                             'birthday': '1983-10-03',
                             'current_team': 'Calgary Flames',
                             'height': '6\' 0',
                             'nationality': 'CAN',
                             'number': '5',
                             'player_id': 8470966,
                             'player_name': 'Mark Giordano',
                             'position': 'Defenseman',
                             'shoots_catches': 'L',
                             'weight': 198}
        _ids = list(Parser.deep_get(self.parser.data, Parser.PLAYER_IDS_PATH).keys())
        player_data = self.parser.get_data_range(_ids, Parser.PLAYER_DATA_PATHS, Parser.PLAYER_DATA_PROCESS)
        self.assertEqual(player_data.iloc[0].to_dict(), expected_info)

    def test_parse_player_data_lastplayer(self):
        expected_info = {'birth_city': 'Turku',
                             'birth_country': 'FIN',
                             'birthday': '1983-03-12',
                             'current_team': 'Minnesota Wild',
                             'height': '6\' 3',
                             'nationality': 'FIN',
                             'number': '9',
                             'player_id': 8469459,
                             'player_name': 'Mikko Koivu',
                             'position': 'Center',
                             'shoots_catches': 'L',
                             'weight': 215}
        _ids = list(Parser.deep_get(self.parser.data, Parser.PLAYER_IDS_PATH).keys())
        player_data = self.parser.get_data_range(_ids, Parser.PLAYER_DATA_PATHS, Parser.PLAYER_DATA_PROCESS)
        self.assertEqual(player_data.iloc[42].to_dict(), expected_info)

    def test_parse_team_data_first(self):
        expected_info = {'abbreviation': 'MIN',
                             'city': 'Minnesota',
                             'conference': 'Western',
                             'division': 'Central',
                             'team_id': 30,
                             'team_name': 'Wild',
                             'time_zone': 'CST',
                             'venue': 'Xcel Energy Center',
                             'year_one': '2000'}
        team_data = self.parser.get_data_range(['home', 'away'], Parser.TEAM_DATA_PATHS)
        self.assertEqual(team_data.iloc[0].to_dict(), expected_info)

    def test_parse_team_data_second(self):
        expected_info = {'abbreviation': 'CGY',
                             'city': 'Calgary',
                             'conference': 'Western',
                             'division': 'Pacific',
                             'team_id': 20,
                             'team_name': 'Flames',
                             'time_zone': 'MST',
                             'venue': 'Scotiabank Saddledome',
                             'year_one': '1980'}
        team_data = self.parser.get_data_range(['home', 'away'], Parser.TEAM_DATA_PATHS)
        self.assertEqual(team_data.iloc[1].to_dict(), expected_info)

    def test_parse_game_events_first(self):
        expected_info = {'away_score': 0,
                             'dateTime': '2016-11-16T00:00:05Z',
                             'description': 'Game Scheduled',
                             'event': 'Game Scheduled',
                             'event_id': 1,
                             'game_id': 2016020237,
                             'home_score': 0,
                             'period': 1,
                             'period_time': '00:00',
                             'periodtype': 'REGULAR',
                             'primary_player': None,
                             'secondary_player': None,
                             'x': None,
                             'y': None}
        play_ids = range(0, len(Parser.deep_get(self.data, Parser.GAME_EVENT_IDS)) + 1)
        game_events = self.parser.get_data_range(play_ids, Parser.GAME_EVENT_PATHS)
        game_events = pd.DataFrame(game_events).replace({np.nan:None})
        self.assertEqual(game_events.iloc[0].to_dict(), expected_info)

    def test_parse_game_events_forth(self):
        expected_info = {'away_score': 0,
                             'dateTime': '2016-11-16T01:08:27Z',
                             'description': 'Eric Staal Wrist Shot saved by Chad Johnson',
                             'event': 'Shot',
                             'event_id': 7,
                             'game_id': 2016020237,
                             'home_score': 0,
                             'period': 1,
                             'period_time': '00:07',
                             'periodtype': 'REGULAR',
                             'primary_player': 8470595.0,
                             'secondary_player': 8473434.0,
                             'x': -61.0,
                             'y': -11.0}
        play_ids = range(0, len(Parser.deep_get(self.data, Parser.GAME_EVENT_IDS)) + 1)
        game_events = self.parser.get_data_range(play_ids, Parser.GAME_EVENT_PATHS)
        game_events = pd.DataFrame(game_events).replace({np.nan:None})
        self.assertEqual(game_events.iloc[4].to_dict(), expected_info)

    def test_parse_game_events_last(self):
        expected_info = {'away_score': 1,
                             'dateTime': '2016-11-16T03:40:44Z',
                             'description': 'Game End',
                             'event': 'Game End',
                             'event_id': 706,
                             'game_id': 2016020237,
                             'home_score': 0,
                             'period': 3,
                             'period_time': '20:00',
                             'periodtype': 'REGULAR',
                             'primary_player': None,
                             'secondary_player': None,
                             'x': None,
                             'y': None}
        play_ids = range(0, len(Parser.deep_get(self.data, Parser.GAME_EVENT_IDS)) + 1)
        game_events = self.parser.get_data_range(play_ids, Parser.GAME_EVENT_PATHS)
        game_events = pd.DataFrame(game_events).replace({np.nan:None})
        self.assertEqual(game_events.iloc[299].to_dict(), expected_info)

    def test_parse_player_stats_first(self):
        expected_info = {'assists': 0,
                             'blocked': 0,
                             'even_time_on_ice': '10:03',
                             'faceoffs_taken': 2,
                             'faceoffs_won': 1,
                             'game_id': 2016020237,
                             'giveaways': 0,
                             'goals': 0,
                             'hits': 3,
                             'penalty_minutes': 0,
                             'player_id': 8469506,
                             'plus_minus': 0,
                             'pp_assists': 0,
                             'pp_goals': 0,
                             'pp_time_on_ice': '0:43',
                             'sh_assists': 0,
                             'sh_goals': 0,
                             'sh_time_on_ice': '0:30',
                             'shots': 2,
                             'takeaways': 0,
                             'time_on_ice': '11:16'}
        player_stats, goalie_stats = self.parser.get_player_stats()
        self.assertEqual(player_stats.iloc[0].to_dict(), expected_info)

    def test_parse_player_stats_second(self):
        expected_info = {'assists': 0,
                             'blocked': 6,
                             'even_time_on_ice': '19:20',
                             'faceoffs_taken': 0,
                             'faceoffs_won': 0,
                             'game_id': 2016020237,
                             'giveaways': 1,
                             'goals': 0,
                             'hits': 0,
                             'penalty_minutes': 0,
                             'player_id': 8474716,
                             'plus_minus': 0,
                             'pp_assists': 0,
                             'pp_goals': 0,
                             'pp_time_on_ice': '2:07',
                             'sh_assists': 0,
                             'sh_goals': 0,
                             'sh_time_on_ice': '5:03',
                             'shots': 4,
                             'takeaways': 1,
                             'time_on_ice': '26:30'}
        player_stats, goalie_stats = self.parser.get_player_stats()
        self.assertEqual(player_stats.iloc[1].to_dict(), expected_info)

    def test_parse_player_stats_goalie(self):
        expected_info = {'assists': 0,
                             'decision': 'L',
                             'even_saves': 16,
                             'even_shots_against': 16,
                             'game_id': 2016020237,
                             'goals': 0,
                             'pim': 0,
                             'player_id': 8471227,
                             'pp_saves': 8,
                             'pp_shots': 9,
                             'saves': 26,
                             'sh_saves': 2,
                             'sh_shots': 2,
                             'shots': 27,
                             'time_on_ice': '58:26'}
        player_stats, goalie_stats = self.parser.get_player_stats()
        self.assertEqual(goalie_stats.iloc[0].to_dict(), expected_info)

    def test_parse_player_stats_last(self):
        expected_info = {'assists': 0,
                             'blocked': 0,
                             'even_time_on_ice': '13:07',
                             'faceoffs_taken': 11,
                             'faceoffs_won': 6,
                             'game_id': 2016020237,
                             'giveaways': 0,
                             'goals': 0,
                             'hits': 0,
                             'penalty_minutes': 2,
                             'player_id': 8470162,
                             'plus_minus': 0,
                             'pp_assists': 0,
                             'pp_goals': 0,
                             'pp_time_on_ice': '0:15',
                             'sh_assists': 0,
                             'sh_goals': 0,
                             'sh_time_on_ice': '0:00',
                             'shots': 0,
                             'takeaways': 0,
                             'time_on_ice': '13:22'}
        player_stats, goalie_stats = self.parser.get_player_stats()
        self.assertEqual(player_stats.iloc[35].to_dict(), expected_info)

    def test_parse_team_stats_first(self):
        expected_info = {'blocked': 13,
                             'faceoffpct': '53.2',
                             'game_id': 2016020237,
                             'giveaways': 14,
                             'goals': 0,
                             'hits': 11,
                             'pim': 19,
                             'pp_goals': 0.0,
                             'pp_oppertunities': 2.0,
                             'pppct': '0.0',
                             'shots': 27,
                             'takeaways': 8,
                             'team_id': 30}
        team_stats = self.parser.get_data_range(['home', 'away'], Parser.TEAM_STATS_PATHS)
        self.assertEqual(team_stats.iloc[0].to_dict(), expected_info)

    def test_parse_team_stats_second(self):
        expected_info = {'blocked': 14,
                             'faceoffpct': '46.8',
                             'game_id': 2016020237,
                             'giveaways': 6,
                             'goals': 1,
                             'hits': 15,
                             'pim': 13,
                             'pp_goals': 1.0,
                             'pp_oppertunities': 6.0,
                             'pppct': '16.7',
                             'shots': 27,
                             'takeaways': 4,
                             'team_id': 20}
        team_stats = self.parser.get_data_range(['home', 'away'], Parser.TEAM_STATS_PATHS)
        self.assertEqual(team_stats.iloc[1].to_dict(), expected_info)

    def test_parse_scratches_first(self):
        expected_info = {'game_id': 2016020237, 'player_id': 8478493}
        scratches = self.parser.get_scratches()
        self.assertEqual(scratches.iloc[0].to_dict(), expected_info)

    def test_parse_scratches_second(self):
        expected_info = {'game_id': 2016020237, 'player_id': 8475613}
        scratches = self.parser.get_scratches()
        self.assertEqual(scratches.iloc[1].to_dict(), expected_info)

    def test_parse_scratches_last(self):
        expected_info = {'game_id': 2016020237, 'player_id': 8475223}
        scratches = self.parser.get_scratches()
        self.assertEqual(scratches.iloc[4].to_dict(), expected_info)

    def test_parse_official_data_first(self):
        expected_info = {'game_id': 2016020237,
                             'official_type': 'Referee',
                             'ref_name': 'Gord Dwyer'}
        ref_ids = range(len(Parser.deep_get(self.parser.data, Parser.OFFICIALS_IDS)))
        officials = self.parser.get_data_range(ref_ids, Parser.OFFICIALS_PATHS)
        self.assertEqual(officials.iloc[0].to_dict(), expected_info)

    def test_parse_official_data_second(self):
        expected_info = {'game_id': 2016020237,
                             'official_type': 'Referee',
                             'ref_name': 'Evgeny Romasko'}
        ref_ids = range(len(Parser.deep_get(self.parser.data, Parser.OFFICIALS_IDS)))
        officials = self.parser.get_data_range(ref_ids, Parser.OFFICIALS_PATHS)
        self.assertEqual(officials.iloc[1].to_dict(), expected_info)

    def test_parse_official_data_last(self):
        expected_info = {'game_id': 2016020237,
                             'official_type': 'Linesman',
                             'ref_name': 'Ryan Galloway'}
        ref_ids = range(len(Parser.deep_get(self.parser.data, Parser.OFFICIALS_IDS)))
        officials = self.parser.get_data_range(ref_ids, Parser.OFFICIALS_PATHS)
        self.assertEqual(officials.iloc[3].to_dict(), expected_info)

    def test_parse_three_stars(self):
        expected_info = {'first_star': [8473434],
                             'game_id': [2016020237],
                             'second_star': [8471227],
                             'third_star': [8476346]}
        stars = self.parser.get_data(Parser.THREE_STARS_PATHS)
        self.assertEqual(stars.to_dict('list'), expected_info)

class DataParserTests(unittest.TestCase):
    def setUp(self):
        url = '.\Tests\\2016020237_testdata.json'
        data = []
        with open(url, 'r') as f:
            data.append(json.load(f))
        self.all_data = Parser.DataParser.parse_data(data)

    def tearDown(self):
        self.all_data = None

    def test_parse_all_check_keys(self):
        expected_info = ['game_info',
                         'goalie_stats',
                         'player_stats',
                         'player_data',
                         'team_data',
                         'game_events',
                         'team_stats',
                         'scratches',
                         'officials',
                         'three_stars']
        self.assertEqual(set(self.all_data.keys()), set(expected_info))

    def test_parse_all_check_threestars(self):
        expected_info = {'first_star': [8473434],
                             'game_id': [2016020237],
                             'second_star': [8471227],
                             'third_star': [8476346]}
        self.assertEqual(set(self.all_data['three_stars'].to_dict('list')), set(expected_info))

if __name__ == '__main__':
    unittest.main()
