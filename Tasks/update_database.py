'''Updates a selected database
Downloads games in batches until there are no more new games for the season
Examples usage:
    python Tasks\update_database.py --file <filename> --batch <batch_size>

TODO:
    Pick batch size based on date of last game and current date
    If last date is the previous season, download the current one
        Or download the previous and start the next season?
    Deal with playoffs
'''
import click
import os
import sys
sys.path.append(os.path.abspath('.'))
from Hipcheck.Hipcheck import Hipcheck
from Hipcheck.Acquirer import GameApiBuilder

LATEST_GAME_QUERY = 'select max(game_id) from game_info'


@click.command()
@click.option(
    '--file', default='test', help='Filename of database to open.'
    )
@click.option(
    '--batch', default=20, help='Batch size to download games with.'
    )
def update_database(file, batch):
    db = Hipcheck(file)
    latest_game = db.post_query(LATEST_GAME_QUERY).loc[0]['max(game_id)']
    _download_games(db, latest_game, batch)


def _download_games(db, game, batch):
    while True:
        start = GameApiBuilder(game)
        end = GameApiBuilder(game + batch)
        print('Downloading: {}'.format((start, end)))
        db.fully_process(start, end)
        game += batch + 1
        if len(db.acquirer.data) != batch + 1:
            break
    print('Download Complete.')

if __name__ == '__main__':
    update_database()
