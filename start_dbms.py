#!/usr/bin/env python
""" Command line interface and interpreter for Hip Check Analytics database
    management system

Provides a way for users to interact with and use the database manager from the
command line.

Example:
    # Starting the interpreter can be done in two ways;
    $ start_dbms.py
    $ start_dbms.py --path Databases/test.db --detail info

    # Commands are taken in the following format
    $ HPA >> command: argument

    # Start a new database at \\Databases\\NHL17.db
    $ HPA >> new database: \\Databases\\NHL17.db

    # Add the 2016-2017 regular season the database
    $ HPA >> set start
    $ Season: 2016
    $ Macthtype: season
    $ Game ID: 1
    $ HPA >> fully process

    # Check the game information for the downloaded data
    $ HPA >> post query: SELECT * FROM game_info
"""
import traceback
import logging
import sys
import pprint
import click
from Hipcheck import Acquirer
from Hipcheck import Hipcheck


class CLI:
    """ CLI for the Hipcheck Analytics project. """
    engine = None
    start = None
    end = None


    def __init__(self, logger=None):
        self.logger = logger or logging.getLogger(__name__)
        self.logger.info('Initializing system.')
        self.prettyPrinter = pprint.PrettyPrinter(indent=4)
        self.engine = Hipcheck.Hipcheck()
        self.build_commands()


    def build_commands(self):
        """
        Build a library of user commands and their corrosponding function calls.
        """
        self.executables = {
            'set start': self.set_start,
            'start': self.set_start,
            'set end': self.set_end,
            'end': self.set_end,
            'acquire data': self.acquire_data,
            'find databases': self.engine.list_databases,
            'open database': self.engine.open_db,
            'database': self.engine.current_db,
            'post query': self.engine.post_query,
            'query': self.engine.post_query,
            'append data': self.engine.append_to_database,
            'parse data': self.engine.parse_data,
            'process data': self.process_data,
            'new database': self.engine.new_database,
            'commit': self.engine.commit_data
        }


    def run(self):
        """ Run through one user command. """
        user_input = input('HPA >> ')
        if user_input.lower() not in ['quit', 'exit']:
            if ':' in user_input:
                command, arguments = user_input.split(':')
            else:
                command, arguments = user_input, None
            self.logger.debug(
                'Command receieved %s, With arguments, %s.', command, arguments
                )
            try:
                if command in self.executables.keys():
                    response = self.executables[command](arguments)
                    if response is not None:
                        self.prettyPrinter.pprint(response)
                else:
                    self.logger.info('Unknown command recieved.')
                self.run()
            except Exception as err:
                traceback.print_exc(file=sys.stdout)
                self.run()
        else:
            self.logger.info('Quitting program')


    def process_data(self, args=None):
        """ Call acquire data with signal to fully process. """
        self.acquire_data(process=True)


    def acquire_data(self, args=None, process=False):
        """ Acquire data, processing if needed. """
        self.logger.info('Acquiring data.')
        if self.start is None:
            self.set_start()
        if process:
            self.engine.fully_process(self.start, self.end)
        else:
            self.engine.acquire_data(self.start, self.end)


    def set_start(self, args=None):
        """ Set the start game for the acquirer. """
        self.logger.info('Setting start game.')
        self.start = self.build_gamecode()
        self.logger.info('Start date set.')


    def set_end(self, args=None):
        """ Set the end game for the acquirer. """
        self.logger.info('Setting end game.')
        self.end = self.build_gamecode()
        self.logger.info('End date set.')


    def build_gamecode(self):
        """ Initialize a GameApiBuilder object using user input. """
        self.logger.info('Building game code.')
        builder = Acquirer.GameApiBuilder()
        builder.set_season(int(input('Season: ')))
        builder.set_matchtype(input('Matchtype: '))
        if builder._matchtype == '03':
            builder.set_round(int(input('Round: ')))
            builder.set_series(int(input('Series: ')))
            builder.set_game(int(input('Game: ')))
        else:
            builder.set_game_id(int(input('Game ID: ')))
        self.logger.info('Code recieved, %s.', builder.get_gamecode())
        return builder

    def open_db(self, path):
        """ Opens database at path. """
        self.logger.info('Opening %s.', path)
        self.engine.open_db(path)
        if self.engine.database_connection is None:
            self.logger.info('Could not open database.')
        else:
            self.logger.info('Successfully opened database.')
        self.run()

def set_logger_detail(detail_level):
    """ Sets the logger configuration for this and all submodules. """
    if detail_level == 'debug':
        detail = logging.DEBUG
    else:
        detail = logging.INFO
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=detail,
        handlers=[
            logging.FileHandler('Logs\\dbms.log'),
            logging.StreamHandler()
        ])
    return logging.getLogger(__name__)

@click.command()
@click.option(
    '--file', default='.\\Databases', help='Filename of database to open.'
    )
@click.option(
    '--detail', default='info', help='Sets detail level of the logger.'
    )
def run_cli(file, detail):
    """ Initialize and run the CLI, act as program entry point. """
    set_logger_detail(detail)
    cli = CLI()
    cli.open_db(file)

if __name__ == '__main__':
    run_cli()
