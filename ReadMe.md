# Hip Check Analytics Database Management System
## What is this?
The Hip Check Analytics database management system (dbms) provides an easy to use system for creating and maintaining sqlite databases of NHL data. The dbms provides a convenient way for the user to use the public NHL api by providing an interface to acquire data within a specified range, as well as parse and append the data to a database. The acquired data includes;

![Alt text](https://bytebucket.org/hyperion4/hca-database-manager/raw/9c812ff657fa0f4b44257f9dcd53bb06154d4319/Documentation/schema.png)
## How does it work?
The dbms works using a command line interface or it can be used as a module in your own code. The NHL provides an api that takes in a few parameters such as season, match type and game ID. To abstract away from working with the URL directly an api builder class is provided, within the command line interface the commands set start and set end are used to start the api builder. At its core the project automates downloading data within the range of two games, or from the starting game ID until no more games are found. The data is then run through a parser where all the relevant data is mined from the JSON payloads and inserted into tables matching the above schema. The system finally appends the parsed data to the database making it ready to be plugged into other applications.

![Alt text](https://bytebucket.org/hyperion4/hca-database-manager/raw/08d3c6a49c5c1fb93dca602d00e1b0af255b9db9/Documentation/nhlapi.png)

Example builder usage;

Game                                                     | Builder Initialization
--------------------------------------------------------:|:-----------------------------------------------------------------------------------:
Regular season game 1, 2016-17                           | `Game_API_Builder().set_season(2016).set_matchtype('season').set_game_id(1)`
Regular season game 460, 2013-14                         | `Game_API_Builder().set_season(2013).set_matchtype('season').set_game_id(460)`
Playoffs quarterfinals series 3 game 1, 2010-11          | `Game_API_Builder().set_season(2010).set_matchtype('playoffs').set_round(2).set_series(3).set_game(1)`
Stanley cup finals game 4, 2016-17                       | `Game_API_Builder().set_season(2016).set_matchtype('playoffs').set_round(4).set_series(1).set_game(4)`

# Installation
### Requirements

Python 3.5 or later

* https://www.python.org/

Sqlite

* https://www.sqlite.org/

From the projects root directory, executing the command `pip install -r requirements.txt` will install all dependencies. Once installation is finished the system is ready to be used.

## Running Tests

A standalone test runner is provided within the main directory of the program named 'run_tests.py' to verify the integrity of the code. Simply running the file with `python runTests.py`  or double clicking it will run all tests and provide a report for the user.

## Running the System

A command line interface is provided to interact with the system. The interface takes two parameters, a database path and level of logging. By default the system will start with no open database if no path is provided as well as initialize a logger with level info if none specified. Example command; 

`python start_dbms.py --path .//Databases//test.db --detail info`

## Interpreter Commands
The command line interface accepts commands in the format;

`HPA >> <command>: <arguements>`

The following are implemented commands;

Commands             | Arguments    | Function
--------------------:|:------------:|:----------------------------------------------------------:
find databases       | path, none   | Lists all databases found within path or current directory if none provided
new database         | path         | Creates and opens a new database at the specified path
open database        | path         | Opens database found at path
database             | none         | Returns the currently opened database
post query           | sql query    | Posts the provided query to the database and prints out the result
set start            | none         | Builds and sets the starting game for the acquire function using user input
set end              | none         | Builds and sets the final game for the acquire function using user input
acquire data         | none         | Downloads all available data as JSON objects starting at the specified start date and ending at the specified end date. If no end date is provided the acquirer will stop at the end of the season if the start is a regular season game, or the end of a playoff series if start is a playoff game
parse data           | none         | Parses the downloaded JSON objects into pandas dataframes, removing redundant and unneeded information as well as grouping the data in a relational manner
append data          | none         | Appends acquired data to the database if there is parsed data ready to append
process data         | none         | Combines acquire, parse and append into one command
quit                 | none         | Quit the interpreter

## Creator
Kyle Beauregard
>* Kylembeauregard@gmail.com
>* https://bitbucket.org/hyperion4